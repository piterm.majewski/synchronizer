package pl.pmajewski.synchronizer.evernote.repository;

import com.evernote.edam.type.Note;
import com.evernote.edam.type.Notebook;

import java.util.List;

public interface EvernoteRemoteRepository {

    List<Notebook> listNotebooks();

    List<Note> listNotes(String notebookGuid);

    Notebook create(Notebook notebook);

    void update(Notebook notebook);

    Note create(Note note);

    Note update(Note note);

    List<Notebook> addToStack(String stackName, List<Notebook> notebooks);

    Note getNote(String guid);
}
