package pl.pmajewski.synchronizer.evernote.repository;

import com.evernote.edam.type.Note;
import com.evernote.edam.type.Notebook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.pmajewski.synchronizer.evernote.client.EvernoteClient;

import java.util.List;

@Component
public class EvernoteRemoteRepositoryImpl implements EvernoteRemoteRepository {

    private EvernoteClient evernoteClient;

    private String authToken;

    public EvernoteRemoteRepositoryImpl(EvernoteClient evernoteClient, @Value("${evernote.token}") String authToken) {
        this.evernoteClient = evernoteClient;
        this.authToken = authToken;
    }

    @Override
    public List<Notebook> listNotebooks() {
        return evernoteClient.listNotebooks(authToken);
    }

    @Override
    public List<Note> listNotes(String notebookGuid) {
        return evernoteClient.listNotes(authToken, notebookGuid);
    }

    @Override
    public Notebook create(Notebook notebook) {
        return evernoteClient.create(authToken, notebook);
    }

    @Override
    public void update(Notebook notebook) {
        evernoteClient.update(authToken, notebook);
    }

    @Override
    public Note create(Note note) {
        return evernoteClient.create(authToken, note);
    }

    @Override
    public Note update(Note note) {
        return evernoteClient.update(authToken, note);
    }

    @Override
    public List<Notebook> addToStack(String stackName, List<Notebook> notebooks) {
        for (Notebook notebook : notebooks) {
            notebook.setStack(stackName);
            evernoteClient.update(authToken, notebook);
        }

        return notebooks;
    }

    @Override
    public Note getNote(String guid) {
        return evernoteClient.getNote(authToken, guid);
    }
}
