package pl.pmajewski.synchronizer.evernote.client;

import com.evernote.edam.type.Note;
import com.evernote.edam.type.Notebook;

import java.util.List;

public interface EvernoteClient {

    List<Notebook> listNotebooks(String token);

    List<Note> listNotes(String token, String notebookGuid);

    Notebook create(String token, Notebook notebook);

    void update(String token, Notebook notebook);

    Note create(String token, Note note);

    Note update(String token, Note note);

    Note getNote(String token, String guid);
}
