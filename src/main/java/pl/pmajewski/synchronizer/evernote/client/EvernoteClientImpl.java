package pl.pmajewski.synchronizer.evernote.client;

import com.evernote.edam.error.EDAMNotFoundException;
import com.evernote.edam.error.EDAMSystemException;
import com.evernote.edam.error.EDAMUserException;
import com.evernote.edam.notestore.NoteFilter;
import com.evernote.edam.type.Note;
import com.evernote.edam.type.Notebook;
import com.evernote.thrift.TException;
import org.springframework.stereotype.Component;
import pl.pmajewski.synchronizer.evernote.exception.EvernoteClientException;

import java.util.List;

@Component
public class EvernoteClientImpl implements EvernoteClient {

    private EvernoteNoteStoreClient noteStoreClient;

    public EvernoteClientImpl(EvernoteNoteStoreClient noteStoreClient) {
        this.noteStoreClient = noteStoreClient;
    }

    @Override
    public List<Notebook> listNotebooks(String token) {
        try {
            return noteStoreClient.get(token).listNotebooks();
        } catch (EDAMUserException | EDAMSystemException | TException e) {
            throw new EvernoteClientException(e);
        }
    }

    @Override
    public List<Note> listNotes(String token, String notebookGuid) {
        NoteFilter noteFilter = new NoteFilter();
        noteFilter.setNotebookGuid(notebookGuid);
        try {
            return noteStoreClient.get(token)
                    .findNotes(noteFilter, 0, Integer.MAX_VALUE)
                    .getNotes();
        } catch (EDAMUserException | EDAMSystemException | EDAMNotFoundException | TException e) {
            throw new EvernoteClientException(e);
        }
    }

    @Override
    public Notebook create(String token, Notebook notebook) {
        try {
            return noteStoreClient.get(token).createNotebook(notebook);
        } catch (EDAMUserException | EDAMSystemException | TException e) {
            throw new EvernoteClientException(e);
        }
    }

    @Override
    public void update(String token, Notebook notebook) {
        try {
            noteStoreClient.get(token).updateNotebook(notebook);
        } catch (EDAMUserException | EDAMSystemException | EDAMNotFoundException | TException e) {
            throw new EvernoteClientException(e);
        }
    }

    @Override
    public Note create(String token, Note note) {
        try {
            return noteStoreClient.get(token)
                    .createNote(note);
        } catch (EDAMUserException | EDAMSystemException | EDAMNotFoundException | TException e) {
            throw new EvernoteClientException(e);
        }
    }

    @Override
    public Note update(String token, Note note) {
        try {
            return noteStoreClient.get(token)
                    .updateNote(note);
        } catch (EDAMUserException | EDAMSystemException | EDAMNotFoundException | TException e) {
            throw new EvernoteClientException(e);
        }
    }

    @Override
    public Note getNote(String token, String guid) {
        try {
            return noteStoreClient.get(token)
                    .getNote(guid, true, true, true, true);
        } catch (EDAMUserException | EDAMSystemException | EDAMNotFoundException | TException e) {
            throw new EvernoteClientException(e);
        }
    }
}
