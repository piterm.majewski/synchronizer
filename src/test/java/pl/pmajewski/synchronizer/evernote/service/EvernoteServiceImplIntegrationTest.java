package pl.pmajewski.synchronizer.evernote.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.pmajewski.synchronizer.evernote.service.model.EvernoteBookmark;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class EvernoteServiceImplIntegrationTest {

    @Autowired
    private EvernoteServiceImpl service;

    @Test
    void shouldAddNewNoteToBookmarkNotebook() {
        EvernoteBookmark bookmark = new EvernoteBookmark("Test Link " + LocalDate.now(), "https://flashcat.io");
        service.save(bookmark);
    }

}